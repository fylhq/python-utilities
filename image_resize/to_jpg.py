#!/usr/bin/env python3

import argparse
import datetime
import os
import re
import glob
from subprocess import Popen, PIPE, run
import concurrent.futures
from multiprocessing import cpu_count

fileRegex = re.compile(r'.*\.(jpg|tiff|tif|png|pdf)', flags=re.IGNORECASE)

def execute(cmd, filePath, args):
	result = run(cmd, stdout=PIPE, stderr=PIPE, shell=True)
						
	if result.returncode != 0:
		print('[ERROR] {} [{}]'.format(filePath, result.stderr), flush=True)
	elif args.verbose or args.very_verbose:
		print('[OK] {}'.format(filePath), flush=True)

def convert(inputDir, outputDir, args):
	print('Base directory: {}'.format(inputDir), flush=True)
	lastTime = datetime.datetime.now()
	lastDir = inputDir

	for dp, _, fn in os.walk(inputDir):
		relDir = os.path.relpath(dp, inputDir)
		if relDir == os.path.curdir: relDir = ""
		cmdAndPath = []
		for file in sorted(fn):
			if fileRegex.match(file):
				filePath = os.path.join(dp, file)
				if lastDir != dp:
					curTime = datetime.datetime.now()
					print("{}: {}".format(dp, curTime - lastTime), flush=True)
					lastDir = dp
					lastTime = curTime
				
				filePair  = os.path.splitext(file)
				outDir = os.path.join(outputDir, relDir)
				outName = os.path.join(outDir, '{}_{:05d}.jpg'.format(filePair[0], args.size))
				
				os.makedirs(outDir, exist_ok=True)
				
				if args.force or not os.path.exists(outName):
					cmd = '{app} -size {size}x{size} "{file}" -resize "{size}x{size}{flag}" -quality {quality} {profile} {progressive} {autoorient} "{out}"'.format(
						app=args.application,
						file=filePath, 
						out=outName, 
						size=args.size, 
						quality=args.quality, 
						flag='' if args.with_smaller else '>',
						profile='+profile "*"' if args.no_exif else '',
						autoorient='-auto-orient' if args.auto_orient else '',
						progressive='-interlace Line' if args.progressive else '')
					if args.dry_run:
						print(cmd)
					else:
						cmdAndPath.append((cmd, filePath))
						
				elif args.very_verbose:
					print('[EXIST] {}'.format(filePath), flush=True)
		if cmdAndPath:
			with concurrent.futures.ThreadPoolExecutor(max_workers=args.threads) as executor:
				fs = { executor.submit(execute, c[0], c[1], args) for c in cmdAndPath }
				concurrent.futures.wait(fs)
				

parser = argparse.ArgumentParser()

parser.add_argument("input_dir", help="Input directory")
parser.add_argument("output_dir", help="Output directory")
parser.add_argument("-a", "--application", help="Application prefix", default="gm convert")
parser.add_argument("-s", "--size", help="Maximum image size in pixels (default: 3072)", type=int, default=3072)
parser.add_argument("-t", "--threads", help="Maximum threads number (default: CPU count)", type=int, default=cpu_count())
parser.add_argument("--with-smaller", help="Allow enlarge small images to fit into the size given", action="store_true")
parser.add_argument("-q", "--quality", help="JPEG quality (default: 80)", type=int, default=75)
parser.add_argument("--auto-orient", help="Rotating Pictures According to Exif Information", action="store_true")
parser.add_argument("--no-exif", help="Remove any ICM, EXIF, IPTC, or other profiles", action="store_true")
parser.add_argument("-n", "--dry-run", help="Do not actually convert images", action="store_true")
parser.add_argument("-f", "--force", help="Force overwrite", action="store_true")
parser.add_argument("-p", "--progressive", help="Progressive JPEG", action="store_true")	
parser.add_argument("-v", "--verbose", help="Verbose output", action="store_true")
parser.add_argument("-vv", "--very-verbose", help="Very verbose output", action="store_true")
args = parser.parse_args()

startTime = datetime.datetime.now()
print('Startup: {}, threads: {}'.format(startTime, args.threads), flush=True)

inputDir = os.path.expanduser(args.input_dir)
outputDir = os.path.expanduser(args.output_dir)

if '*' in inputDir:
	dirs = glob.glob(inputDir)
	commonDir = os.path.commonprefix([os.path.dirname(dir) for dir in dirs])
	print('Common glob input directory: {}'.format(commonDir), flush=True)
	for inDir in dirs:
		outDir = os.path.join(args.output_dir, os.path.relpath(inDir, commonDir))
		convert(inDir, outDir, args)
else:
	convert(inputDir, outputDir, args)

finishTime = datetime.datetime.now()
print('Finish: {}'.format(finishTime))
print('Total time: {}'.format(finishTime - startTime))